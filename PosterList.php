<?php

require_once "Poster.php";

Class PosterList{
    const DIRECTORY = "assets";
    const DEFAULT_SORTING_METHOD = "ASC";
    const AVAILABLE_SORTING_METHODS = ['ASC', 'DESC'];

    private $sortingMethod = '';
    private $posterEntityList = [];

    public function getPosters():array{
        $this->loadPosters()->sortPosterEntityList();
        return $this->posterEntityList;
    }

    public function loadPosters():PosterList{
        //Potential trouble, because we dont know is there all images in .png format.
        //In a real project, we would receive data from the database, so we can avoid this trouble in query.
        $posters = glob(self::DIRECTORY."/*.png");
        if (!empty($posters)){
            foreach ($posters AS $path){
                $entity = $this->getPosterEntityNew()->loadByPath(strval($path));
                $this->posterEntityList[$entity->getId()] = $entity;
            }
        }
        return $this;
    }
    private function sortPosterEntityList():PosterList{
        if ($this->getSortingMethod() == 'ASC'){
            ksort($this->posterEntityList);
        }else{
            krsort($this->posterEntityList);
        }
        return $this;
    }
    private function getPosterEntityNew():Poster{
        return new Poster();
    }
    //Getters & Setters
    public function setSortingMethod(string $sortMethod):PosterList{
        if (in_array(strtoupper($sortMethod), self::AVAILABLE_SORTING_METHODS)){
            $this->sortingMethod = strtoupper($sortMethod);
        }else{
            $this->sortingMethod = self::DEFAULT_SORTING_METHOD;
        }

        return $this;
    }
    public function getSortingMethod():string{
        return (!empty($this->sortingMethod)) ? $this->sortingMethod : self::DEFAULT_SORTING_METHOD;
    }
}