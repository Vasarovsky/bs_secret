<?php

Class CollageGenerator{
    const SPACE_BETWEEN_POSTERS = 10;
    const COLLAGE_WIDTH = 1850; //40 + 1810; => 5 Posters + 4 spaces between them;
    const COLLAGE_HEIGHT = 1098; //10 + (544 * 2);  => 2 Posters height + 10 px between rows;
    const POSTERS_IN_ROW = 5;
    const DIRECTORY = "assets/collages";

    private $posterList = [];

    public function generate():array{
        if (empty($this->posterList))
            return ['status' => false, 'msg' => 'empty_collage_list', 'path' => ''];
        //New img generation width and height calculated by data from documentation.
        //If the width was different we need to calculate MAX ROW width before create an img.
        $collage = imagecreatetruecolor(self::COLLAGE_WIDTH, self::COLLAGE_HEIGHT);
        //Set transparent background
        imagesavealpha($collage, true);
        $pngTransparency = imagecolorallocatealpha($collage , 0, 0, 0, 127);
        imagefill($collage , 0, 0, $pngTransparency);

        $coordinateX = 0;
        $coordinateY = 0;
        $currentPostersInRow = 0;
        foreach ($this->posterList AS $posterEntity){
            $poster = imagecreatefrompng($posterEntity->getPath());
            imagecopy($collage, $poster, $coordinateX, $coordinateY, 0, 0, $posterEntity->getWidth(), $posterEntity->getHeight());
            imagedestroy($poster);
            $coordinateX += ($posterEntity->getWidth() + self::SPACE_BETWEEN_POSTERS);
            $currentPostersInRow++;
            if ($currentPostersInRow == self::POSTERS_IN_ROW){
                $coordinateX = 0;
                //We can get any poster height, because all of them are the same. Information from documentation;
                $coordinateY = $posterEntity->getHeight() + self::SPACE_BETWEEN_POSTERS;
            }
        }

        $path = self::DIRECTORY.'/'.(new \DateTime())->getTimestamp().'.png';
        imagepng($collage, $path);
        imagedestroy($collage);
        return ['status' => true, 'msg' => 'generated', 'path' => $path];
    }

    public function appendPoster(\Poster $poster):CollageGenerator{
        $this->posterList[] = $poster;
        return $this;
    }
    public function appendPosterList(array $posterList):CollageGenerator{
        if (!empty($posterList)){
            foreach ($posterList AS $posterEntity){
                if ($posterEntity instanceof Poster){
                    $this->posterList[] = $posterEntity;
                }
            }
        }
        return $this;
    }
}