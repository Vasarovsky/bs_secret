<?php

Class Poster{
    private $id = 0;
    private $path = '';
    private $width = 362;
    private $height = 544;

    public function loadByPath(string $path):Poster{
        //In real project we will ask for data from DB here.
        //P.S. If only one entity needed.
        //If we need more, than request would be in PosterList class, but current class would be extended from some base class,
        //for example Entity, where in turn setXXX() could be made for all fields without additional requests.
        $this->setPath($path);
        $this->setId(preg_replace('/[^0-9]+/', '', $path));
        return $this;
    }
    public function getId():int{
        return $this->id;
    }
    public function setId(int $id){
        $this->id = $id;
        return $this;
    }
    public function getPath():string{
        return $this->path;
    }
    public function setPath(string $path):Poster{
        $this->path = $path;
        return $this;
    }
    public function getWidth():int{
        return $this->width;
    }
    public function setWidth(int $width):Poster{
        $this->width = $width;
        return $this;
    }
    public function getHeight():int{
        return $this->height;
    }
    public function setHeight(int $height):Poster{
        $this->height = $height;
        return $this;
    }
}