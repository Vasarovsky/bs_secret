<?php
require_once "Controller.php";
$response = (new Controller())->generateCollage();
if (!$response['status']){
    echo $response['msg'];
}else{
    echo 'Collage saved in: ' . $response['path'];
    echo '<img src="'.$response['path'].'">';
}

