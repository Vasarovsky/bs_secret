<?php

require_once "PosterList.php";
require_once "CollageGenerator.php";

Class Controller{

    public function generateCollage():array{
        return $this->getCollageGeneratorEntity()->appendPosterList($this->getPosters())->generate();
    }

    public function getPosters():array{
        return $this->getPosterListEntity()->setSortingMethod('ASC')->getPosters();
    }

    private function getPosterListEntity():PosterList{
        return new PosterList();
    }
    private function getCollageGeneratorEntity():CollageGenerator{
        return new CollageGenerator();
    }
}